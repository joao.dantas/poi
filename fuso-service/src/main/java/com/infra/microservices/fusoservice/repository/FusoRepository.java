package com.infra.microservices.fusoservice.repository;

import com.infra.microservices.fusoservice.model.Fuso;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FusoRepository extends JpaRepository<Fuso, Long> {

    Fuso findFirstByTimeZoneContainsIgnoreCase(String timeZone);

    Fuso findFirstByCommentsContainsIgnoreCase(String comements);

}
