package com.infra.microservices.fusoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FusoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FusoServiceApplication.class, args);
	}

}
