package com.infra.microservices.fusoconversionservice.proxy;

import com.infra.microservices.fusoconversionservice.model.Fuso;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "fuso", url = "${FUSO_URI:http://localhost}:8000")
public interface FusoProxy {

    @GetMapping("/fuso/from/{country}")
    Fuso retrieveTimeZoneFromCountry(@PathVariable String country) ;
}
