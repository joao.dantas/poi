package com.infra.microservices.fusoservice.service;

import com.infra.microservices.fusoservice.model.Fuso;
import com.infra.microservices.fusoservice.repository.FusoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FusoServiceTests {

    @InjectMocks
    private FusoService service;

    @Mock
    private FusoRepository repository;

    @Test
    void checkSucess() {
        when(repository.findFirstByTimeZoneContainsIgnoreCase("Recife"))
                .thenReturn(
                        new Fuso(101L,
                                "BR",
                                "America/Recife",
                                "Pernambuco",
                                "-03:00",
                                "-03:00",
                                "")
                );

        Fuso fuso = service.retrieveTimeZoneFromCountry("Recife");

        Assertions.assertEquals("America/Recife", fuso.getTimeZone());
        Assertions.assertEquals("-03:00", fuso.getUtc());
    }

    @Test
    void checkFail() {
        when(repository.findFirstByTimeZoneContainsIgnoreCase("abc")).thenReturn(null);

        RuntimeException thrown = Assertions.assertThrows(RuntimeException.class, () -> service.retrieveTimeZoneFromCountry("abc"));

        Assertions.assertEquals("Unable to find locaton called abc.", thrown.getMessage());
    }

}
