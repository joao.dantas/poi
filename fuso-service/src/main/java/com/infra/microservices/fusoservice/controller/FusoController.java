package com.infra.microservices.fusoservice.controller;


import com.infra.microservices.fusoservice.model.Fuso;
import com.infra.microservices.fusoservice.service.FusoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RestController;

@RestController("fuso")
public class FusoController {

    @Autowired
    private FusoService service;

    @GetMapping("/fuso/from/{country}")
    public Fuso retrieveTimeZoneFromCountry(@PathVariable String country) {

        return service.retrieveTimeZoneFromCountry(country);
    }
}
