package com.infra.microservices.fusoservice.service;

import com.infra.microservices.fusoservice.model.Fuso;
import com.infra.microservices.fusoservice.repository.FusoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FusoService {

    @Autowired
    private FusoRepository repository;

    public Fuso retrieveTimeZoneFromCountry(String country) {

        Fuso fuso = repository.findFirstByTimeZoneContainsIgnoreCase(country);

        if(fuso == null) {
            fuso = repository.findFirstByCommentsContainsIgnoreCase(country);
        }

        if (fuso == null) {
            throw new RuntimeException(String.format("Unable to find locaton called %s.", country));
        }

        return fuso;

    }
}
