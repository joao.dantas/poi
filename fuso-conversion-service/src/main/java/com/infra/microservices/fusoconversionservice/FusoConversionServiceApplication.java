package com.infra.microservices.fusoconversionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FusoConversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FusoConversionServiceApplication.class, args);
	}

}
