package com.infra.microservices.fusoservice.model;

import javax.persistence.*;

@Entity
public class Fuso {

    @Id
    private Long id;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "TIME_ZONE")
    private String timeZone;

    @Column(name = "COMMENTS")
    private String comments;

    @Column(name = "UTC")
    private String utc;

    @Column(name = "UTC_DST")
    private String utcDst;

    @Column(name = "NOTES")
    private String notes;

    public Fuso() {}

    public Fuso(Long id, String countryCode, String timeZone, String comments, String utc, String utcDst, String notes) {
        this.id = id;
        this.countryCode = countryCode;
        this.timeZone = timeZone;
        this.comments = comments;
        this.utc = utc;
        this.utcDst = utcDst;
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUtc() {
        return utc;
    }

    public void setUtc(String utc) {
        this.utc = utc;
    }

    public String getUtcDst() {
        return utcDst;
    }

    public void setUtcDst(String utcDst) {
        this.utcDst = utcDst;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
