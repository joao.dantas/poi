package com.infra.microservices.fusoconversionservice.service;

import com.infra.microservices.fusoconversionservice.model.Fuso;
import com.infra.microservices.fusoconversionservice.model.FusoConversion;
import com.infra.microservices.fusoconversionservice.proxy.FusoProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeParseException;

@Service
public class FusoConversionService {

    @Autowired
    private FusoProxy proxy;

    public FusoConversion calculateFusoConversion(@PathVariable String from, @PathVariable String to, @PathVariable String hour) {
        FusoConversion result;
        try {
            Fuso fusoOrigin = proxy.retrieveTimeZoneFromCountry(from);
            Fuso fusoDestiny = proxy.retrieveTimeZoneFromCountry(to);

            ZoneOffset offsetFrom = ZoneOffset.ofHours(Integer.parseInt(fusoOrigin.getUtc().split(":")[0]));
            ZoneOffset offsetTo = ZoneOffset.ofHours(Integer.parseInt(fusoDestiny.getUtc().split(":")[0]));

            LocalTime time = OffsetTime.of(LocalTime.parse(hour),  offsetFrom).withOffsetSameInstant(offsetTo).toLocalTime();

            result = new FusoConversion(from, to, hour, time.toString());
        } catch (DateTimeParseException e) {
            throw new RuntimeException("Formato invalido de data.");
        } catch (Exception e) {
            throw new RuntimeException("Falha ao adiquiri os fusos, por favor revisar o nome das cidades.");
        }
        return result;
    }

}
