package com.infra.microservices.fusoconversionservice.model;

import java.math.BigDecimal;

public class FusoConversion {

    private String from;

    private String to;

    private String hour;

    private String result;

    public FusoConversion() {

    }

    public FusoConversion(String from, String to, String hour, String result) {
        this.from = from;
        this.to = to;
        this.hour = hour;
        this.result = result;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
