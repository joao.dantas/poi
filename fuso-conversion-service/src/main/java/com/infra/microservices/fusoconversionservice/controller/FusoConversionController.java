package com.infra.microservices.fusoconversionservice.controller;

import com.infra.microservices.fusoconversionservice.model.FusoConversion;
import com.infra.microservices.fusoconversionservice.service.FusoConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FusoConversionController {

    @Autowired
    private FusoConversionService service;

    @GetMapping("/fuso-conversion/from/{from}/to/{to}/hour/{hour}")
    public FusoConversion calculateFusoConversion(@PathVariable String from, @PathVariable String to, @PathVariable String hour) {
        return service.calculateFusoConversion(from, to, hour);
    }
}
